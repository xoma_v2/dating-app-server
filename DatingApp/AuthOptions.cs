﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace DatingApp
{
    public class AuthOptions
    {
        // Издатель токена.
        public const string ISSUER = "MyAuthServer";
        // Потребитель токена.
        public const string AUDIENCE = "https://localhost:5001/";
        // Ключ для шифрации.
        const string KEY = "ultrasuperdupersecretkey_321_123_!";
        // Время жизни токена (в минутах).
        public const int LIFETIME = 60;

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}