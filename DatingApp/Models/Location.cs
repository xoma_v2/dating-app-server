﻿using System;

namespace DatingApp.Models
{
    public class Location
    {
        /// <summary> Широта </summary>
        public double Latitude { get; set; }
        /// <summary> Долгота </summary>
        public double Longitude { get; set; }

        public double GetDistanceTo(Location other)
        {
            if (double.IsNaN(Latitude) || double.IsNaN(Longitude) || double.IsNaN(other.Latitude) || double.IsNaN(other.Longitude))
                throw new ArgumentException("Argument latitude or longitude is not a number");

            var radiansOverDegrees = (Math.PI / 180.0);

            var d1 = Latitude * radiansOverDegrees;
            var num1 = Longitude * radiansOverDegrees;
            var d2 = other.Latitude * radiansOverDegrees;
            var num2 = other.Longitude * radiansOverDegrees - num1;
            var d3 = Math.Pow(Math.Sin((d2 - d1) / 2.0), 2.0) +
                     Math.Cos(d1) * Math.Cos(d2) * Math.Pow(Math.Sin(num2 / 2.0), 2.0);

            return 6376500.0 * (2.0 * Math.Atan2(Math.Sqrt(d3), Math.Sqrt(1.0 - d3)));
        }
    }
}
