﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DatingApp.Models
{
    public class UserTest
    {
        public long Id { get; set; }
        public long UserId { get; set; }       // В результате получим внешний ключ со свойствами
        public virtual User User { get; set; } // NOT NULL и ON DELETE CASCADE.
        public string Answers { get; set; }

        /// <summary>
        /// Чем меньше результат, тем выше совместимость партнёров.
        /// </summary>
        /// <param name="men"> Ответы мужщины. </param>
        /// <param name="women"> Ответы женщины. </param>
        /// <returns></returns>
        public static double Analysis(string men, string women)
        {
            List<int> m = new List<int>();
            foreach (var c in men)
                m.Add(Convert.ToInt32(c));

            List<int> w = new List<int>();
            foreach (var c in women)
                w.Add(Convert.ToInt32(c));

            return Analysis(m, w);
        }

        /// <summary>
        /// Чем меньше результат, тем выше совместимость партнёров.
        /// </summary>
        /// <param name="men"> Ответы мужщины. </param>
        /// <param name="women"> Ответы женщины. </param>
        /// <returns></returns>
        public static double Analysis(List<int> men, string women)
        {
            List<int> w = new List<int>();
            foreach (var c in women)
                w.Add(Convert.ToInt32(c));

            return Analysis(men, w);
        }

        /// <summary>
        /// Чем меньше результат, тем выше совместимость партнёров.
        /// </summary>
        /// <param name="men"> Ответы мужщины. </param>
        /// <param name="women"> Ответы женщины. </param>
        /// <returns></returns>
        public static double Analysis(string men, List<int> women)
        {
            List<int> m = new List<int>();
            foreach (var c in men)
                m.Add(Convert.ToInt32(c));

            return Analysis(m, women);
        }

        /// <summary>
        /// Чем меньше результат, тем выше совместимость партнёров.
        /// </summary>
        /// <param name="men"> Ответы мужщины. </param>
        /// <param name="women"> Ответы женщины. </param>
        /// <returns></returns>
        public static double Analysis(List<int> men, List<int> women)
        {
            // >>> Семейные ценности:
            // Интимно-сексуальная
            int m1 = men[0] + men[1] + men[2];
            int w1 = women[0] + women[1] + women[2];

            // Личностная идентификация
            int m2 = men[3] + men[4] + men[5];
            int w2 = women[3] + women[4] + women[5];

            // Хозяйственно-бытовая
            double m3 = (men[6] + men[7] + men[8] + men[21] + men[22] + men[23]) / 2.0;
            double w3 = (women[6] + women[7] + women[8] + women[21] + women[22] + women[23]) / 2.0;

            // Родительско-воспитательная
            double m4 = (men[9] + men[10] + men[11] + men[24] + men[25] + men[26]) / 2.0;
            double w4 = (women[9] + women[10] + women[11] + women[24] + women[25] + women[26]) / 2.0;

            // Социальная активность
            double m5 = (men[12] + men[13] + men[14] + men[27] + men[28] + men[29]) / 2.0;
            double w5 = (women[12] + women[13] + women[14] + women[27] + women[28] + women[29]) / 2.0;

            // Эмоционально-психотерапевтическая
            double m6 = (men[15] + men[16] + men[17] + men[30] + men[31] + men[32]) / 2.0;
            double w6 = (women[15] + women[16] + women[17] + women[30] + women[31] + women[32]) / 2.0;

            // Внешняя привлекательность
            double m7 = (men[18] + men[19] + men[20] + men[33] + men[34] + men[35]) / 2.0;
            double w7 = (women[18] + women[19] + women[20] + women[33] + women[34] + women[35]) / 2.0;

            // >>> Согласованность семейных ценностей супругов.
            double res = (Math.Abs(m1 - w1) + Math.Abs(m2 - w2) +
                          Math.Abs(m3 - w3) + Math.Abs(m4 - w4) +
                          Math.Abs(m5 - w5) + Math.Abs(m6 - w6) +
                          Math.Abs(m7 - w7)) / 7.0;

            // >>> Ролевая адекватность мужа.
            double resM = Math.Abs(women[21] + women[22] + women[23] - men[6] - men[7] - men[8]) +
                          Math.Abs(women[24] + women[25] + women[26] - men[9] - men[10] - men[11]) +
                          Math.Abs(women[27] + women[28] + women[29] - men[12] - men[13] - men[14]) +
                          Math.Abs(women[30] + women[31] + women[32] - men[15] - men[16] - men[17]) +
                          Math.Abs(women[33] + women[34] + women[35] - men[18] - men[19] - men[20]);

            // >>> Ролевая адекватность жены.
            double resW = Math.Abs(men[21] + men[22] + men[23] - women[6] - women[7] - women[8]) +
                          Math.Abs(men[24] + men[25] + men[26] - women[9] - women[10] - women[11]) +
                          Math.Abs(men[27] + men[28] + men[29] - women[12] - women[13] - women[14]) +
                          Math.Abs(men[30] + men[31] + men[32] - women[15] - women[16] - women[17]) +
                          Math.Abs(men[33] + men[34] + men[35] - women[18] - women[19] - women[20]);

            return Math.Abs(resW - resM) * res;
        }
    }
}
