﻿namespace DatingApp.Models
{
    public class UserPhoto
    {
        public long Id { get; set; }
        public long UserId { get; set; }       // В результате получим внешний ключ со свойствами
        public virtual User User { get; set; } // NOT NULL и ON DELETE CASCADE.
        public byte[] Image { get; set; }
        public bool IsMain { get; set; }
    }
}
