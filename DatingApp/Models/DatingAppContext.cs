﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DatingApp.Models
{
    public class DatingAppContext : DbContext
    {
        public DbSet<User> User { get; set; }
        public DbSet<UserProfile> UserProfile { get; set; }
        public DbSet<UserTest> UserTest { get; set; }
        public DbSet<UserPhoto> UserPhoto { get; set; }
        public DbSet<Favorite> Favorite { get; set; }
        //public DbSet<Location> Location { get; set; }
        //public DbSet<Message> Message { get; set; }

        public DatingAppContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new UserProfileConfiguration());
            modelBuilder.ApplyConfiguration(new UserTestConfiguration());
            modelBuilder.ApplyConfiguration(new UserPhotoConfiguration());
            modelBuilder.ApplyConfiguration(new FavoriteConfiguration());
            //modelBuilder.ApplyConfiguration(new LocationConfiguration());
            //modelBuilder.ApplyConfiguration(new MessageConfiguration());
        }
    }

    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder
                .HasOne(u => u.Profile)
                .WithOne(p => p.User)
                .HasForeignKey<UserProfile>(p => p.UserId);
            builder
                .HasOne(u => u.Test)
                .WithOne(p => p.User)
                .HasForeignKey<UserTest>(p => p.UserId);
            builder
                .Property(u => u.Login)
                .IsRequired()
                .HasMaxLength(25);
            builder
                .HasAlternateKey(u => u.Login);
            builder
                .Property(u => u.Password)
                .IsRequired();
            //builder
            //    .Ignore(u => u.Preference);
        }
    }

    public class UserProfileConfiguration : IEntityTypeConfiguration<UserProfile>
    {
        public void Configure(EntityTypeBuilder<UserProfile> builder)
        {
            builder
                .Property(p => p.UserId)
                .IsRequired();
            builder
                .HasIndex(p => p.Email)
                .IsUnique();
            builder
                .HasIndex(p => p.Phone)
                .IsUnique();
            builder
                .Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(25);
            builder
                .Property(p => p.Surname)
                .IsRequired()
                .HasMaxLength(25);
            builder
                .Property(p => p.Sex)
                .IsRequired();
            builder
                .Property(p => p.Birthday)
                .IsRequired();
            builder
                .OwnsOne(p => p.Location);
        }
    }

    public class UserTestConfiguration : IEntityTypeConfiguration<UserTest>
    {
        public void Configure(EntityTypeBuilder<UserTest> builder)
        {
            builder
                .Property(t => t.UserId)
                .IsRequired();
            builder
                .Property(t => t.Answers)
                .IsRequired();
        }
    }

    public class UserPhotoConfiguration : IEntityTypeConfiguration<UserPhoto>
    {
        public void Configure(EntityTypeBuilder<UserPhoto> builder)
        {
            builder
                .HasOne(uph => uph.User)
                .WithMany(u => u.Photos)
                .HasForeignKey(uph => uph.UserId)
                .IsRequired();
            builder
                .Property(uph => uph.Image)
                .IsRequired();
            builder
                .Property(uph => uph.IsMain)
                .IsRequired();
        }
    }

    public class FavoriteConfiguration : IEntityTypeConfiguration<Favorite>
    {
        public void Configure(EntityTypeBuilder<Favorite> builder)
        {
            builder
                .HasOne(f => f.UserFrom)
                .WithMany(u => u.FavoritesFrom)
                .HasForeignKey(f => f.UserFromId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
            builder
                .HasOne(f => f.UserTo)
                .WithMany(u => u.FavoritesTo)
                .HasForeignKey(f => f.UserToId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
            builder
                .Property(f => f.AddTime)
                .HasComputedColumnSql("GETDATE()")
                .IsRequired();
        }
    }

    /*public class LocationConfiguration : IEntityTypeConfiguration<Location>
    {
        public void Configure(EntityTypeBuilder<Location> builder)
        {
            builder
                .Property(l => l.Latitude)
                .IsRequired();
            builder
                .Property(l => l.Longitude)
                .IsRequired();
        }
    }*/

    /*public class MessageConfiguration : IEntityTypeConfiguration<Message>
    {
        public void Configure(EntityTypeBuilder<Message> builder)
        {
            builder
                .Property(m => m.UserFromId)
                .IsRequired();
            builder
                .Property(m => m.UserToId)
                .IsRequired();
            builder
                .Property(m => m.Text)
                .IsRequired();
            builder
                .Property(m => m.SendTime)
                .HasComputedColumnSql("GETDATE()")
                .IsRequired();
            builder
                .Property(m => m.Seen)
                .HasDefaultValue(false)
                .IsRequired();
            builder
                .Property(m => m.DeletedFrom)
                .HasDefaultValue(false)
                .IsRequired();
            builder
                .Property(m => m.DeletedTo)
                .HasDefaultValue(false)
                .IsRequired();
        }
    }*/
}
