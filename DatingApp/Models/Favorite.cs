﻿using System;

namespace DatingApp.Models
{
    public class Favorite
    {
        public long Id { get; set; }
        public long UserFromId { get; set; }
        public virtual User UserFrom { get; set; }
        public long UserToId { get; set; }
        public virtual User UserTo { get; set; }
        public DateTime AddTime { get; set; }
    }
}
