﻿using System;

namespace DatingApp.Models
{
    public class UserProfile
    {
        public long Id { get; set; }
        public long UserId { get; set; }       // В результате получим внешний ключ со свойствами
        public virtual User User { get; set; } // NOT NULL и ON DELETE CASCADE.
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public char Sex { get; set; }
        public DateTime Birthday { get; set; }
        public byte? AgeFrom { get; set; }
        public byte? AgeTo { get; set; }
        public Location Location { get; set; }

        public UserProfile() { }

        public UserProfile(UserProfile profile)
        {
            Id = profile.Id;
            UserId = profile.UserId;
            User = profile.User;
            Email = profile.Email;
            Phone = profile.Phone;
            Name = profile.Name;
            Surname = profile.Surname;
            Sex = profile.Sex;
            Birthday = profile.Birthday;
            AgeFrom = profile.AgeFrom;
            AgeTo = profile.AgeTo;
            Location = profile.Location;
        }
    }
}
