﻿using System.Collections.Generic;

namespace DatingApp.Models
{
    public class User
    {
        public long Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public virtual UserProfile Profile { get; set; }
        public virtual UserTest Test { get; set; }
        public virtual List<UserPhoto> Photos { get; set; }
        public virtual List<Favorite> FavoritesFrom { get; set; }
        public virtual List<Favorite> FavoritesTo { get; set; }
    }
}