﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DatingApp.Models
{
    public class FullProfile : UserProfile
    {
        public List<byte[]> Photos { get; set; }
        public bool IsFavorite { get; set; }

        public FullProfile()
        {
            Photos = new List<byte[]>();
            IsFavorite = false;
        }

        public FullProfile(UserProfile profile) : base(profile)
        {
            Photos = new List<byte[]>();
            IsFavorite = false;
        }
    }
}
