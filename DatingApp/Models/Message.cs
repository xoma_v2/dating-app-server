﻿using System;

namespace DatingApp.Models
{
    public class Message
    {
        public long Id { get; set; }
        public long UserFromId { get; set; }
        public virtual User UserFrom { get; set; }
        public long UserToId { get; set; }
        public virtual User UserTo { get; set; }
        public string Text { get; set; }
        public DateTime SendTime { get; set; }
        public bool Seen { get; set; }
        public bool DeletedFrom { get; set; }
        public bool DeletedTo { get; set; }
    }
}
