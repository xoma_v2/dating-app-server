﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DatingApp.Library;
using DatingApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Newtonsoft.Json;
using System.Security.Claims;

namespace DatingApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly DatingAppContext db;

        public AccountsController(DatingAppContext context)
        {
            db = context;
        }

        // POST api/accounts/{user_login}?password={user_password}
        [HttpPost("{login}")]
        public async Task Post(string login, [FromQuery] User prms)
        {
            var identity = GetIdentity(login, PasswordHashing.Get(prms.Password));
            if (identity == null)
            {
                Response.StatusCode = 400;
                await Response.WriteAsync("Invalid username or password.");
                return;
            }

            var now = DateTime.UtcNow;
            // создаем JWT-токен
            var jwt = new JwtSecurityToken(
                issuer: AuthOptions.ISSUER,
                audience: AuthOptions.AUDIENCE,
                notBefore: now,
                claims: identity.Claims,
                expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var response = new
            {
                access_token = encodedJwt,
                user_id = identity.Name
            };

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        private ClaimsIdentity GetIdentity(string login, string password)
        {
            User user = db.User.FirstOrDefault(x => x.Login == login && x.Password == password);
            if (user != null)
            {
                var claims = new List<Claim> { new Claim(ClaimsIdentity.DefaultNameClaimType, user.Id.ToString()) };
                ClaimsIdentity claimsIdentity =
                    new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

                return claimsIdentity;
            }

            // если пользователя не найдено
            return null;
        }

        // GET api/accounts/{user_login}
        [HttpGet("{login}")]
        public ActionResult<bool> Get(string login)
        {
            int count = (from user in db.User
                         where user.Login == login
                         select user).Count();
            if (count != 0)
                return false;
            else
                return true;
        }

        // POST api/accounts?login={user_login}&password={user_password}
        [HttpPost]
        public ActionResult<bool> Post([FromQuery] User prms)
        {
            int count = (from user in db.User
                         where user.Login == prms.Login
                         select user).Count();
            if (count != 0)
                return false;

            db.User.Add(new User { Login = prms.Login, Password = PasswordHashing.Get(prms.Password) });
            db.SaveChanges();

            return true;
        }
    }
}
