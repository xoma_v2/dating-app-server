﻿using DatingApp.Library;
using DatingApp.Models;
using Microsoft.AspNetCore.Mvc;

namespace DatingApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GeneratorsController : ControllerBase
    {
        private readonly DatingAppContext db;

        public GeneratorsController(DatingAppContext context)
        {
            db = context;
        }

        // POST api/generators/filler
        [HttpPost("filler")]
        public ActionResult<string> PostFiller()
        {
            TableFiller filler = new TableFiller(db);
            filler.Fill(5000);

            return "Похоже, что генератор завершил свою работу.";
        }
    }
}
