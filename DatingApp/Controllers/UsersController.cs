﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DatingApp.Library;
using DatingApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DatingApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly DatingAppContext db;

        public UsersController(DatingAppContext context)
        {
            db = context;
        }

        // Вернуть список ближайщих к пользователю партнёров.
        // GET api/users/{user_id}/closest
        //[Authorize]
        [HttpGet("{id}/closest")]
        public ActionResult<IEnumerable<long>> GetIdClosest(long id)
        {
            UserProfile partner = (from profile in db.UserProfile
                                   where profile.UserId == id
                                   select profile).Single();
            var result = (from profile in db.UserProfile
                          where profile.UserId != partner.UserId && profile.Sex != partner.Sex
                          orderby profile.Location.GetDistanceTo(partner.Location)
                          select profile.UserId).ToList();

            return result;
        }

        // Вернуть список партнёров пользователя с {user_id}, подобранных с использованием алгоритма марьяжа.
        // GET api/users/{user_id}/marriage
        //[Authorize]
        [HttpGet("{id}/marriage")]
        public ActionResult<IEnumerable<long>> GetIdMarriage(long id)
        {
            UserProfile partner = (from profile in db.UserProfile
                                   where profile.UserId == id
                                   select profile).Single();
            Marriage mr = new Marriage(db, 150, 1000);
            //Marriage mr = new Marriage(db);

            return mr.FindPairs(partner);
        }

        // Вернуть информацию о существовании пользователей с номером телефона {user_phone}.
        // GET api/users?phone={user_phone}
        //[Authorize]
        [HttpGet]
        public ActionResult<bool> Get_Phone([FromQuery] string phone)
        {
            int count = (from profile in db.UserProfile
                         where profile.Phone == phone
                         select profile).Count();
            if (count != 0)
                return false;
            else
                return true;
        }

        // Вернуть информацию о существовании пользователей с электронной почтой {user_email}.
        // GET api/users?email={user_email}
        //[Authorize]
        [HttpGet]
        public ActionResult<bool> Get_Email([FromQuery] string email)
        {
            int count = (from profile in db.UserProfile
                         where profile.Email == email
                         select profile).Count();
            if (count != 0)
                return false;
            else
                return true;
        }

        // Вернуть данные о пользователе по его id.
        // GET api/users/{user_id}?withphotos=true
        // GET api/users/{user_id}?withphotos=false
        //[Authorize]
        [HttpGet("{id}")]
        public ActionResult<FullProfile> GetId_Withphotos(long id, [FromBody] bool withphotos = false)
        {
            try
            {
                FullProfile result = new FullProfile((from profile in db.UserProfile
                                                      where profile.UserId == id
                                                      select profile).Single());
                if (withphotos)
                    result.Photos = (from photo in db.UserPhoto
                                     where photo.UserId == result.UserId
                                     orderby photo.IsMain descending
                                     select ImageOperations.Decompress(photo.Image)).ToList();
                else
                    result.Photos = null;

                return result;
            }
            catch
            {
                return null;
            }
        }

        // Вернуть данные о пользователе с {user_id} с информацией, нравится ли он пользователю с { favoritefrom_id }.
        // GET api/users/{user_id}?favoritefrom={favoritefrom_id}
        //[Authorize]
        [HttpGet("{target}")]
        public ActionResult<FullProfile> GetId_Favoritefrom(long target, [FromQuery(Name = "favoritefrom_id")]long sender)
        {
            FullProfile result = (from profile in db.UserProfile
                                  where profile.UserId == target
                                  select new FullProfile(profile)).Single();
            result.Photos = (from photo in db.UserPhoto
                             where photo.UserId == result.UserId
                             orderby photo.IsMain descending
                             select ImageOperations.Decompress(photo.Image)).ToList();
            result.IsFavorite = (from favorite in db.Favorite
                                 where favorite.UserFromId == sender && favorite.UserToId == target
                                 select favorite).Count() != 0 ? true : false;

            return result;
        }

        /*// GET api/datingapp/linkprofile/5/31
        [Authorize]
        [HttpGet("linkprofile/{target}/{sender}")]
        public ActionResult<FullProfile> GetLinkProfile(long target, long sender)
        {
            FullProfile result = (from profile in db.UserProfile
                                  where profile.UserId == target
                                  select new FullProfile(profile)).Single();
            result.Photos = (from photo in db.UserPhoto
                             where photo.UserId == result.UserId
                             orderby photo.IsMain descending
                             select Encoding.ASCII.GetBytes(photo.Id.ToString())).ToList();
            result.IsFavorite = (from favorite in db.Favorite
                                 where favorite.UserFromId == sender && favorite.UserToId == target
                                 select favorite).Count() != 0 ? true : false;

            return result;
        }*/

        //Вернуть данные об ответах пользователя с { user_id }:
        //GET https://localhost:5001/api/users/{user_id}/answers
        //Вернуть данные о пользователях, отмеченных как понравившиеся, пользователя с {user_id}:
        //GET https://localhost:5001/api/users/{user_id}/favorites
        //Вернуть данные о пользователях, которым нравится пользователь с {favoriteto_id}:
        //GET https://localhost:5001/api/users?favoriteto={favoriteto_id}
        //Вернуть данные о пользователях, которым нравится пользователь с {favoriteto_id}, с пагинацией:
        //GET https://localhost:5001/api/users?favoriteto={favoriteto_id}&offset=0&limit=15


        // Создать нового пользователя в разделе users.
        // POST api/users
        [Authorize]
        [HttpPost]
        public void PostProfile([FromBody] FullProfile profile)
        {
            UserProfile pr = new UserProfile
            {
                UserId = profile.UserId,
                Email = profile.Email == "" ? null : profile.Email,
                Phone = profile.Phone == "" ? null : profile.Phone,
                Name = profile.Name,
                Surname = profile.Surname,
                Sex = profile.Sex,
                Birthday = profile.Birthday,
                AgeFrom = profile.AgeFrom,
                AgeTo = profile.AgeTo,
                Location = profile.Location
            };
            db.UserProfile.Add(pr);
            db.SaveChanges();

            if (profile.Photos != null && profile.Photos.Count > 0)
            {
                List<UserPhoto> ph = new List<UserPhoto>();

                foreach (var photo in profile.Photos)
                    ph.Add(new UserPhoto { UserId = profile.UserId, Image = photo, IsMain = false });

                db.UserPhoto.AddRange(ph);
                db.SaveChanges();
            }
        }


        //Добавить пользователю с {user_id} нового любимого пользователя с { favoriteto_id }:
        //POST https://localhost:5001/api/users/{user_id}/favorites/{favoriteto_id}
        //Добавить пользователю с {user_id} данные об его ответах:
        //POST https://localhost:5001/api/users/{user_id}/answers 
        //Обновить данные об ответах пользователя с { user_id }:
        //PUT https://localhost:5001/api/users/{user_id}/answers
        //Удалить из раздела "Понравившиеся" пользователя с { user_id }
        //пользователя с { another_id }:
        //DELETE https://localhost:5001/api/users/{user_id}/favorites/{another_id}
    }
}
