﻿using DatingApp.Library;
using DatingApp.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using System.IO;
using System.Drawing;
using System.Net;

namespace DatingApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DatingAppController : ControllerBase
    {
        private readonly DatingAppContext db;

        public DatingAppController(DatingAppContext context)
        {
            db = context;
        }

        // GET api/datingapp/marriage/5
        [Authorize]
        [HttpGet("marriage/{id}")]
        public ActionResult<IEnumerable<long>> GetMarriage(long id)
        {
            UserProfile partner = (from profile in db.UserProfile
                                   where profile.UserId == id
                                   select profile).Single();
            Marriage mr = new Marriage(db, 150, 1000);
            //Marriage mr = new Marriage(db);

            return mr.FindPairs(partner);
        }

        // GET api/datingapp/closest/5
        [Authorize]
        [HttpGet("closest/{id}")]
        public ActionResult<IEnumerable<long>> GetClosest(long id)
        {
            UserProfile partner = (from profile in db.UserProfile
                                   where profile.UserId == id
                                   select profile).Single();
            var result = (from profile in db.UserProfile
                          where profile.UserId != partner.UserId && profile.Sex != partner.Sex
                          orderby profile.Location.GetDistanceTo(partner.Location)
                          select profile.UserId).ToList();

            return result;
        }

        // GET api/datingapp/check/phone/+79999735755
        [Authorize]
        [HttpGet("check/phone/{phone}")]
        public ActionResult<bool> CheckPhone(string phone)
        {
            int count = (from profile in db.UserProfile
                         where profile.Phone == phone
                         select profile).Count();
            if (count != 0)
                return false;
            else
                return true;
        }

        // GET api/datingapp/check/email/xomav2@gmail.com
        [Authorize]
        [HttpGet("check/email/{email}")]
        public ActionResult<bool> CheckEmail(string email)
        {
            int count = (from profile in db.UserProfile
                         where profile.Email == email
                         select profile).Count();
            if (count != 0)
                return false;
            else
                return true;
        }

        // GET api/datingapp/profile/5
        [Authorize]
        [HttpGet("profile/{id}")]
        public ActionResult<FullProfile> GetProfile(long id)
        {
            FullProfile result = new FullProfile((from profile in db.UserProfile
                                                  where profile.UserId == id
                                                  select profile).Single());
            result.Photos = null;

            return result;
        }

        // GET api/datingapp/fullprofile/5
        [Authorize]
        [HttpGet("fullprofile/{id}")]
        public ActionResult<FullProfile> GetFullProfile(long id)
        {
            try
            {
                FullProfile result = (from profile in db.UserProfile
                                      where profile.UserId == id
                                      select new FullProfile(profile)).Single();
                result.Photos = (from photo in db.UserPhoto
                                 where photo.UserId == result.UserId
                                 orderby photo.IsMain descending
                                 select ImageOperations.Decompress(photo.Image)).ToList();

                return result;
            }
            catch
            {
                return null;
            }
        }

        // PUT api/datingapp/profile
        [Authorize]
        [HttpPut("fullprofile")]
        public void PutFullProfile([FromBody] FullProfile profile)
        {
            UserProfile pr = new UserProfile
            {
                UserId = profile.UserId,
                Email = profile.Email == "" ? null : profile.Email,
                Phone = profile.Phone == "" ? null : profile.Phone,
                Name = profile.Name,
                Surname = profile.Surname,
                Sex = profile.Sex,
                Birthday = profile.Birthday,
                AgeFrom = profile.AgeFrom,
                AgeTo = profile.AgeTo,
                Location = profile.Location
            };
            db.UserProfile.Add(pr);
            db.SaveChanges();

            if (profile.Photos != null && profile.Photos.Count > 0)
            {
                List<UserPhoto> ph = new List<UserPhoto>();

                foreach (var photo in profile.Photos)
                    ph.Add(new UserPhoto { UserId = profile.UserId, Image = photo, IsMain = false });

                db.UserPhoto.AddRange(ph);
                db.SaveChanges();
            }
        }

        // GET api/datingapp/fullprofile/5/31
        [Authorize]
        [HttpGet("fullprofile/{target}/{sender}")]
        public ActionResult<FullProfile> GetFullProfile(long target, long sender)
        {
            FullProfile result = (from profile in db.UserProfile
                                  where profile.UserId == target
                                  select new FullProfile(profile)).Single();
            result.Photos = (from photo in db.UserPhoto
                             where photo.UserId == result.UserId
                             orderby photo.IsMain descending
                             select ImageOperations.Decompress(photo.Image)).ToList();
            result.IsFavorite = (from favorite in db.Favorite
                                 where favorite.UserFromId == sender && favorite.UserToId == target
                                 select favorite).Count() != 0 ? true : false;

            return result;
        }

        // GET api/datingapp/linkprofile/5/31
        [Authorize]
        [HttpGet("linkprofile/{target}/{sender}")]
        public ActionResult<FullProfile> GetLinkProfile(long target, long sender)
        {
            FullProfile result = (from profile in db.UserProfile
                                  where profile.UserId == target
                                  select new FullProfile(profile)).Single();
            result.Photos = (from photo in db.UserPhoto
                             where photo.UserId == result.UserId
                             orderby photo.IsMain descending
                             select Encoding.ASCII.GetBytes(photo.Id.ToString())).ToList();
            result.IsFavorite = (from favorite in db.Favorite
                                 where favorite.UserFromId == sender && favorite.UserToId == target
                                 select favorite).Count() != 0 ? true : false;

            return result;
        }

        // GET api/datingapp/photo/2377
        [Authorize]
        [HttpGet("photo/{id}")]
        public IActionResult GetPhoto(long id)
        {
            byte[] result = (from photo in db.UserPhoto
                             where photo.Id == id
                             select ImageOperations.Decompress(photo.Image)).Single();

            return File(result, "image/jpeg");
        }

        // PUT api/datingapp/favorite/5/31
        [Authorize]
        [HttpPut("favorite/{target}/{sender}")]
        public void PutFavorite(long target, long sender)
        {
            List<Favorite> result = (from favorite in db.Favorite
                                     where favorite.UserFromId == sender && favorite.UserToId == target
                                     orderby favorite.AddTime descending
                                     select favorite).ToList();

            if (result.Count() == 0)
            {
                db.Favorite.Add(new Favorite { UserFromId = sender, UserToId = target });
                db.SaveChanges();
            }
            else if (result.Count() > 1)
            {
                db.Favorite.RemoveRange(result.Except(new List<Favorite> { result.First() }));
                db.SaveChanges();
            }
        }

        // DELETE api/datingapp/favorite/5/31
        [Authorize]
        [HttpDelete("favorite/{target}/{sender}")]
        public void DeleteFavorite(long target, long sender)
        {
            List<Favorite> result = (from favorite in db.Favorite
                                     where favorite.UserFromId == sender && favorite.UserToId == target
                                     select favorite).ToList();

            db.Favorite.RemoveRange(result);
            db.SaveChanges();
        }

        // POST api/datingapp/fullprofiles/31
        [Authorize]
        [HttpPost("fullprofiles/{sender}")]
        public ActionResult<IEnumerable<FullProfile>> GetFullProfiles(long sender, [FromBody] List<long> targets)
        {
            List<FullProfile> resultList = new List<FullProfile>();
            foreach (var target in targets)
            {
                FullProfile result = (from profile in db.UserProfile
                                      where profile.UserId == target
                                      select new FullProfile(profile)).Single();
                result.Photos = (from photo in db.UserPhoto
                                 where photo.UserId == result.UserId
                                 orderby photo.IsMain descending
                                 select ImageOperations.Decompress(photo.Image)).ToList();
                result.IsFavorite = (from favorite in db.Favorite
                                     where favorite.UserFromId == sender && favorite.UserToId == target
                                     select favorite).Count() != 0 ? true : false;
                resultList.Add(result);
            }

            return resultList;
        }

        // GET api/dating/answers/64
        [Authorize]
        [HttpGet("answers/{id}")]
        public ActionResult<IEnumerable<int>> GetAnswers(long id)
        {
            try
            {
                string test_Answers = (from t in db.UserTest
                                       where t.UserId == id
                                       select t.Answers).Single();
                List<int> builder = new List<int>();
                foreach (var c in test_Answers)
                    builder.Add(int.Parse(c.ToString()));

                return builder;
            }
            catch
            {
                return null;
            }
        }

        // POST api/datingapp/answers/5
        [Authorize]
        [HttpPost("answers/{id}")]
        public void PostAnswers(long id, [FromBody] List<int> answers)
        {
            List<UserTest> test = (from t in db.UserTest
                                   where t.UserId == id
                                   select t).ToList();
            db.UserTest.RemoveRange(test);

            StringBuilder builder = new StringBuilder();
            foreach (var c in answers)
                builder.Append(c);
            db.UserTest.Add(new UserTest { UserId = id, Answers = builder.ToString() });

            db.SaveChanges();
        }

        // PUT api/datingapp/answers/5
        [Authorize]
        [HttpPut("answers/{id}")]
        public void PutAnswers(long id, [FromBody] List<int> answers)
        {
            UserTest test = (from t in db.UserTest
                             where t.UserId == id
                             select t).Single();
            StringBuilder builder = new StringBuilder();
            foreach (var c in answers)
                builder.Append(c);

            test.Answers = builder.ToString();
            db.UserTest.Update(test);
            db.SaveChanges();
        }

        //[Authorize]
        [HttpGet("favoriteprofiles/{id}")]
        public ActionResult<IEnumerable<FullProfile>> GetFavoriteProfiles(long id)
        {
            List<long> favorites = (from f in db.Favorite
                                    where f.UserFromId == id
                                    orderby f.AddTime descending
                                    select f.UserToId).ToList();
            List<UserProfile> profiles = (from profile in db.UserProfile
                                          where favorites.Contains(profile.UserId) == true
                                          select profile).ToList();
            List<FullProfile> result = new List<FullProfile>();
            foreach (var profile in profiles)
            {
                FullProfile fprofile = new FullProfile(profile);
                fprofile.IsFavorite = true;
                fprofile.Photos = (from photo in db.UserPhoto
                                   where photo.UserId == fprofile.UserId
                                   orderby photo.IsMain descending
                                   select ImageOperations.Decompress(photo.Image)).ToList();
                result.Add(fprofile);
            }

            return result;
        }

        [Authorize]
        [HttpGet("favoriteprofiles/{id}/{indexFrom}/{indexTo}")]
        public ActionResult<IEnumerable<FullProfile>> GetFavoriteProfiles(long id, int indexFrom, int indexTo)
        {
            List<long> favorites = (from f in db.Favorite
                                    where f.UserFromId == id
                                    orderby f.AddTime descending
                                    select f.UserToId).ToList();
            if (indexFrom > indexTo)
            {
                int tmp = indexTo;
                indexTo = indexFrom;
                indexFrom = tmp;
            }
            if ((favorites.Count() - 1) < indexFrom)
                return new List<FullProfile>();
            else if ((indexTo - indexFrom) == 0)
                return new List<FullProfile>();
            else if (favorites.Count() < indexTo)
                indexTo = favorites.Count();
            favorites = favorites.GetRange(indexFrom, indexTo - indexFrom);
            List<UserProfile> profiles = (from profile in db.UserProfile
                                          where favorites.Contains(profile.UserId) == true
                                          select profile).ToList();
            List<FullProfile> result = new List<FullProfile>();
            foreach (var profile in profiles)
            {
                FullProfile fprofile = new FullProfile(profile);
                fprofile.IsFavorite = true;
                fprofile.Photos = (from photo in db.UserPhoto
                                   where photo.UserId == fprofile.UserId
                                   orderby photo.IsMain descending
                                   select ImageOperations.Decompress(photo.Image)).ToList();
                result.Add(fprofile);
            }

            return result;
        }

        [Authorize]
        [HttpGet("favorites/{id}")]
        public ActionResult<IEnumerable<long>> GetFavorites(long id)
        {
            List<long> result = (from f in db.Favorite
                                 where f.UserFromId == id
                                 orderby f.AddTime descending
                                 group f by f.UserToId into g
                                 select g.Key).ToList();

            return result;
        }

        /*// POST api/datingapp
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/datingapp/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/datingapp/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }*/
    }
}
