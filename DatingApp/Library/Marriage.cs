﻿using DatingApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DatingApp.Library
{
    // C# program for stable marriage problem 
    public class Marriage
    {
        public List<string> debug = new List<string>();

        private class PotentialPartner
        {
            public long UserId;
            public string Answers;
            public List<PotentialPartner> Preferences;
        }
        private readonly DatingAppContext db;
        private UserProfile partner;
        private int returnCount;
        private int N; // Number of Men or Women

        public Marriage(DatingAppContext context, int returnCount = int.MaxValue, int N = int.MaxValue)
        {
            db = context;
            this.returnCount = returnCount;
            this.N = N;
        }

        public List<long> FindPairs(UserProfile partner)
        {
            this.partner = partner;

            //db.User.Select(user => user.Profile.Sex == 'm').Count();
            int mCount = (from profile in db.UserProfile
                          where profile.Sex == 'm'
                          select profile).Count();
            int wCount = (from profile in db.UserProfile
                          where profile.Sex == 'w'
                          select profile).Count();

            if (mCount > wCount)
            {
                if (N > wCount)
                    N = wCount;
            }
            else if (N > mCount)
                N = mCount;

            //N = mCount > wCount ? wCount : mCount;

            List<PotentialPartner> men = (from profile in db.UserProfile
                                          join test in db.UserTest on profile.UserId equals test.UserId
                                          where profile.Sex == 'm'
                                          orderby profile.Location.GetDistanceTo(partner.Location)
                                          select new PotentialPartner { UserId = profile.UserId, Answers = test.Answers }).Take(N).ToList();
            List<PotentialPartner> women = (from profile in db.UserProfile
                                            join test in db.UserTest on profile.UserId equals test.UserId
                                            where profile.Sex == 'w'
                                            orderby profile.Location.GetDistanceTo(partner.Location)
                                            select new PotentialPartner { UserId = profile.UserId, Answers = test.Answers }).Take(N).ToList();
            
            foreach (var m in men)
            {
                List<int> answers = new List<int>();
                foreach (var c in m.Answers)
                    answers.Add(Convert.ToInt32(c));

                m.Preferences = (from w in women
                                 orderby UserTest.Analysis(answers, w.Answers)
                                 select w).ToList();
            }
            foreach (var w in women)
            {
                List<int> answers = new List<int>();
                foreach (var c in w.Answers)
                    answers.Add(Convert.ToInt32(c));

                w.Preferences = (from m in men
                                 orderby UserTest.Analysis(m.Answers, answers)
                                 select m).ToList();
            }

            List<PotentialPartner> index;
            List<PotentialPartner> offer;
            List<long> result = new List<long>();

            if (partner.Sex == 'm')
            {
                index = men;
                offer = women;
            }
            else
            {
                index = women;
                offer = men;
            }

            while (index.Count > 1)
            {
                long offerId = StableMarriage(index, offer);
                PotentialPartner indexId = index.Last();

                N--;

                index.Remove(indexId);
                foreach (var m in index)
                    foreach (var p in m.Preferences)
                        if (p.UserId == offerId)
                        {
                            m.Preferences.Remove(p);
                            break;
                        }

                foreach (var w in offer)
                    if (w.UserId == offerId)
                    {
                        offer.Remove(w);
                        break;
                    }
                foreach (var w in offer)
                    w.Preferences.Remove(indexId);

                result.Add(offerId);

                if (--returnCount == 0)
                    return result;
            }

            result.Add(offer.First().UserId);
            return result;
        }

        // This function returns true if woman 'w' prefers man 'm1' over man 'm' 
        private bool IsM1OverM(PotentialPartner w, long m, long m1)
        {
            // Check if w prefers m over her current engagment m1 
            foreach (var p in w.Preferences)
            {
                // If m1 comes before m in lisr of w, then w prefers her 
                // current engagement, don't do anything 
                if (p.UserId == m1)
                    return true;

                // If m comes before m1 in w's list, then free her current 
                // engagement and engage her with m 
                if (p.UserId == m)
                    return false;
            }

            return false;
        }

        // Prints stable matching for N boys and N girls. Boys are numbered as 0 to 
        // N-1. Girls are numbereed as N to 2N-1. 
        private long StableMarriage(List<PotentialPartner> index, List<PotentialPartner> offer)
        {
            // Stores partner of women. This is our output array that 
            // stores paing information. The value of wPartner[i] 
            // indicates the partner assigned to woman N+i. Note that 
            // the woman numbers between N and 2*N-1. The value -1 
            // indicates that (N+i)'th woman is free
            Dictionary<long, long> wPartner = new Dictionary<long, long>(N);

            // An array to store availability of men. If mFree[i] is 
            // false, then man 'i' is free, otherwise engaged. 
            Dictionary<long, bool> mFree = new Dictionary<long, bool>(N);

            // Initialize all men and women as free
            foreach (var w in offer)
                wPartner.Add(w.UserId, -1);
            foreach (var m in index)
                mFree.Add(m.UserId, false);
            int freeCount = N;

            // While there are free men 
            while (freeCount > 0)
            {
                // Pick the first free man (we could pick any) 
                PotentialPartner m = null;
                foreach (var t in index)
                    if (mFree[t.UserId] == false)
                    {
                        m = t;
                        break;
                    }

                // One by one go to all women according to m's preferences. 
                // Here m is the picked free man 
                for (int i = 0; i < N && mFree[m.UserId] == false; i++)
                {
                    PotentialPartner w = m.Preferences[i];

                    // The woman of preference is free, w and m become 
                    // partners (Note that the partnership maybe changed 
                    // later). So we can say they are engaged not married 
                    if (wPartner[w.UserId] == -1)
                    {
                        wPartner[w.UserId] = m.UserId;
                        mFree[m.UserId] = true;
                        freeCount--;
                    }
                    else // If w is not free 
                    {
                        // Find current engagement of w 
                        long m1_UserId = wPartner[w.UserId];

                        // If w prefers m over her current engagement m1, 
                        // then break the engagement between w and m1 and 
                        // engage m with w. 
                        if (IsM1OverM(w, m.UserId, m1_UserId) == false)
                        {
                            wPartner[w.UserId] = m.UserId;
                            mFree[m.UserId] = true;
                            mFree[m1_UserId] = false;
                        }
                    } // End of Else 
                } // End of the for loop that goes to all women in m's list 
            } // End of main while loop

            long offerId = -1;

            foreach (var p in wPartner)
                if (p.Value == partner.UserId)
                {
                    offerId = p.Key;
                    break;
                }

            return offerId;
        }
    }
}