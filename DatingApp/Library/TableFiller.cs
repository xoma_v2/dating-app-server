﻿using DatingApp.Library;
using DatingApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.IO;

namespace DatingApp.Library
{
    public class TableFiller
    {
        private DatingAppContext db;
        private DataGenerator generator;

        public TableFiller(DatingAppContext context)
        {
            db = context;
            generator = new DataGenerator();
        }

        private void FillUser(int count)
        {
            List<string> logins = generator.genLoginSet(count, 5, 20);
            
            foreach (var login in logins)
            {
                string password = generator.genPasswordHash(5, 20);

                db.User.Add(new User { Login = login, Password = password });
            }

            db.User.Add(new User { Login = "xoma_v2", Password = "TU1SaePH9KMjQ9iW2MBVlfYc6yiXamShCrlQawqjcrc=" });

            db.SaveChanges();
        }

        private void FillUserProfile()
        {
            List<string> phones = generator.genPhoneSet(db.User.Count(), "+7");
            List<string> emails = generator.genEmailSet(db.User.Count());
            int i = 0;

            foreach (var user in db.User)
            {
                UserProfile profile = new UserProfile();

                profile.UserId = user.Id;
                profile.Email = emails[i];
                profile.Phone = phones[i];
                profile.Sex = generator.genSex();
                if (profile.Sex == 'm')
                {
                    profile.Name = generator.genMaleName();
                    profile.Surname = generator.genMaleSurname();
                }
                else
                {
                    profile.Name = generator.genFemaleName();
                    profile.Surname = generator.genFemaleSurname();
                }
                profile.Birthday = generator.genDate();
                profile.AgeFrom = (byte?)generator.genNumber(18, 100);
                profile.AgeTo = (byte?)generator.genNumber(18, 100);
                if (profile.AgeTo < profile.AgeFrom)
                {
                    var tmp = profile.AgeTo;
                    profile.AgeTo = profile.AgeFrom;
                    profile.AgeFrom = tmp;
                }
                profile.Location = generator.genLocation();

                i++;
                db.UserProfile.Add(profile);
            }

            db.SaveChanges();
        }

        private void FillUserTest()
        {
            foreach (var user in db.User)
            {
                string answers = "";

                for (int i = 0; i < 36; i++)
                    answers += generator.genNumber(0, 4).ToString();

                db.UserTest.Add(new UserTest { UserId = user.Id, Answers = answers });
            }

            db.SaveChanges();
        }

        private List<byte[]> DownloadImages(int count = 200)
        {
            object lockMe = new object();
            HashSet<byte[]> images = new HashSet<byte[]>();
            int j = 0;

            Parallel.For(0, count, (i) =>
            {
                // Адрес ресурса, к которому выполняется запрос
                string url = "https://thispersondoesnotexist.com/image";

                // Создаём объект WebClient
                using (var webClient = new WebClient())
                {
                    try
                    {
                        // Выполняем запрос по адресу и получаем ответ в виде массива байтов
                        byte[] response = webClient.DownloadData(url);
                        response = ImageOperations.CreateThumbnail(response, 300);
                        response = ImageOperations.Compress(response);
                        if (images.Add(response) == true)
                            lock (lockMe)
                            {
                                Console.WriteLine(j);
                                j++;
                            }
                    }
                    catch (Exception) { }
                }
            });

            return images.ToList();
        }

        private void FillUserPhoto()
        {
            List<byte[]> images = DownloadImages();
            List<long> db_UserPhoto = (from photo in db.UserPhoto
                                       group photo by photo.UserId into p
                                       select p.Key).ToList();
            List<long> db_User = (from user in db.User
                                  where db_UserPhoto.Contains(user.Id) == false
                                  select user.Id).ToList();

            foreach (var user_Id in db_User)
            {
                int count = generator.genNumber(0, 4);
                int mainPhoto = generator.genNumber(0, count);
                if (images.Count < count)
                    images = DownloadImages();

                for (int i = 0; i < count; i++)
                {
                    byte[] image = images.First();
                    db.UserPhoto.Add(new UserPhoto { UserId = user_Id, Image = image, IsMain = i == mainPhoto ? true : false });
                    images.Remove(image);
                    db.SaveChanges();
                }
            }
        }

        private void FillFavorite()
        {
            object lockMe = new object();
            var senders = (from profile in db.UserProfile
                           select new { profile.UserId, profile.Sex }).ToList();
            var targets = (from profile in db.UserProfile
                           select new { profile.UserId, profile.Sex }).ToList();

            Parallel.ForEach(senders, (sender) =>
            {
                var partners = (from target in targets
                                where target.Sex != sender.Sex && target.UserId != sender.UserId
                                orderby target.UserId
                                select target).ToList();
                int count = generator.genNumber(0, 301);
                count = count > partners.Count ? partners.Count : count;

                for (int i = 0; i < count; i++)
                {
                    var partner = partners[generator.genNumber(0, partners.Count)];
                    partners.Remove(partner);

                    lock (lockMe)
                    {
                        db.Favorite.Add(new Favorite { UserFromId = sender.UserId, UserToId = partner.UserId });
                        db.SaveChanges();
                    }
                }
            });
        }

        //delete from[UserPhoto]
        //where[Id] in
        //    (
        //        select T.[Id]
        //        from[UserPhoto] as T join
        //           (
        //               select[Image]
        //               from [UserPhoto]
        //                group by [Image]
        //                having COUNT([Image]) > 1
        //	        ) as S on T.[Image] = S.[Image]
        //        where T.[Id] not in
        //            (
        //                select min([Id])
        //                from[UserPhoto]
        //
        //                group by[Image]
        //
        //                having count([Image]) > 1
	    //            )
	    //    )
        //public void Func()
        //{
        //    Parallel.ForEach(db.UserPhoto, (image) =>
        //    {
        //        byte[] img = image.Image;
        //        img = ImageOperations.Decompress(img);
        //        img = ImageOperations.CreateThumbnail(img, 300);
        //        img = ImageOperations.Compress(img);
        //        image.Image = img;
        //        db.UserPhoto.Update(image);
        //    });
        //
        //    db.SaveChanges();
        //}

        public void Fill(int count = 100)
        {
            //FillUser(count);
            //FillUserProfile();
            //FillUserTest();
            //FillUserPhoto();
            //FillFavorite();
            //Func();
        }
    }
}
