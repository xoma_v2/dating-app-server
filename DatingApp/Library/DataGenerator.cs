﻿using DatingApp.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DatingApp.Library
{
    public class DataGenerator
    {
        private readonly Random random;
        private readonly List<string> maleSurname;
        private readonly List<string> femaleSurname;

        public DataGenerator()
        {
            random = new Random();

            List<string> all = new List<string>();
            using (var reader = new StreamReader(@"D:\Users\Никита\source\repos\DatingApp\DatingApp\Library\russian_surnames.csv"))
            {
                if (!reader.EndOfStream)
                    reader.ReadLine();

                while (!reader.EndOfStream)
                    all.Add(reader.ReadLine().Split(';')[1]);
            }

            maleSurname = new List<string>();
            maleSurname.Add(all[0]);
            for (int i = 1; i < all.Count; i++)
            {
                if ((all[i] + "a") == all[i - 1])
                    continue;
                else if (all[i].EndsWith("ва") || all[i].EndsWith("ая") || all[i].EndsWith("на"))
                    continue;
                else
                    maleSurname.Add(all[i]);
            }

            femaleSurname = new List<string>();
            for (int i = 0; i < all.Count - 1; i++)
            {
                if (all[i] == (all[i + 1] + "a"))
                    continue;
                else if (all[i].EndsWith("ов") || all[i].EndsWith("ев") || all[i].EndsWith("ий"))
                    continue;
                else
                    femaleSurname.Add(all[i]);
            }
        }

        public string genRandomLine(int size)
        {
            if (size < 1)
                throw new Exception("Argument exception");

            char[] alphabet = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
                                           'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                                           'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
                                           'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                                           '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '_', '!' };
            char[] str = new char[size];

            for (int i = 0; i < size; i++)
                str[i] = alphabet[random.Next(alphabet.Length)];

            return new string(str);
        }
        
        public string genRandomLine(int min, int max)
        {
            if (min > max || min < 1)
                throw new Exception("Argument exception");

            return genRandomLine(random.Next(min, max + 1));
        }
        
        public string genPasswordHash(int size)
        {
            if (size < 1)
                throw new Exception("Argument exception");

            string password = genRandomLine(size);
            return PasswordHashing.Get(password);
        }
        
        public string genPasswordHash(int min, int max)
        {
            if (min > max || min < 1)
                throw new Exception("Argument exception");

            string password = genRandomLine(random.Next(min, max + 1));
            return PasswordHashing.Get(password);
        }
        
        public List<string> genLoginSet(int count, int min, int max)
        {
            if (count < 1 || min > max || min < 1)
                throw new Exception("Argument exception");

            HashSet<string> set = new HashSet<string>(count);

            for (int i = 0, j = 0; i < count; j++)
            {
                if (set.Add(genRandomLine(min, max + 1)) == true)
                    i++;

                if (j > (3 * count))
                    throw new Exception("Infinite loop");
            }

            return set.ToList();
        }

        public string genMaleName()
        {
            List<string> male = new List<string>();

            using (var reader = new StreamReader(@"D:\Users\Никита\source\repos\DatingApp\DatingApp\Library\russian_names.csv"))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] values = line.Split(';');

                    if (values[2] == "М")
                        male.Add(values[1]);
                }
            }

            return male[random.Next(male.Count)];
        }

        public string genFemaleName()
        {
            List<string> female = new List<string>();

            using (var reader = new StreamReader(@"D:\Users\Никита\source\repos\DatingApp\DatingApp\Library\russian_names.csv"))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] values = line.Split(';');

                    if (values[2] == "Ж")
                        female.Add(values[1]);
                }
            }

            return female[random.Next(female.Count)];
        }

        public string genMaleSurname()
        {
            return maleSurname[random.Next(maleSurname.Count)];
        }

        public string genFemaleSurname()
        {
            return femaleSurname[random.Next(femaleSurname.Count)];
        }

        public string genEmail()
        {
            char[] alphabet = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
                                           'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                                           'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
                                           'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                                           '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '.' };
            int size = random.Next(6, 31);
            string[] address = new string[] { "mail", "google", "rambler", "yandex", "yahoo" };
            string[] domain = new string[] { ".md", ".ru", ".ua", ".com", ".net", ".org", ".jp", ".eu", ".cn" };
            StringBuilder email = new StringBuilder();

            for (int i = 0; i < size; i++)
                email.Append(alphabet[random.Next(alphabet.Length)]);
            email.Append("@");
            email.Append(address[random.Next(address.Length)]);
            email.Append(domain[random.Next(address.Length)]);

            return email.ToString();
        }

        public List<string> genEmailSet(int count)
        {
            if (count < 1)
                throw new Exception("Argument exception");

            HashSet<string> set = new HashSet<string>(count);

            for (int i = 0, j = 0; i < count; j++)
            {
                if (set.Add(genEmail()) == true)
                    i++;

                if (j > (3 * count))
                    throw new Exception("Infinite loop");
            }

            return set.ToList();
        }

        public string genPhone(string countryCode)
        {
            StringBuilder number = new StringBuilder();

            for (int i = 0; i < 10; i++)
                number.Append(random.Next(10));

            return countryCode + number.ToString();
        }

        public List<string> genPhoneSet(int count, string countryCode)
        {
            if (count < 1)
                throw new Exception("Argument exception");

            HashSet<string> set = new HashSet<string>(count);

            for (int i = 0, j = 0; i < count; j++)
            {
                if (set.Add(genPhone(countryCode)) == true)
                    i++;

                if (j > (3 * count))
                    throw new Exception("Infinite loop");
            }

            return set.ToList();
        }

        public DateTime genDate()
        {
            int year = random.Next(1900, 2002);
            int month = random.Next(1, 13);
            int day = random.Next(1, DateTime.DaysInMonth(year, month) + 1);

            return new DateTime(year, month, day);
        }

        public char genSex()
        {
            return random.Next(2) == 1 ? 'm' : 'w';
        }

        public int genNumber(int fromInclusive, int toExclusive)
        {
            if (fromInclusive > toExclusive)
                return random.Next(toExclusive, fromInclusive);
            else
                return random.Next(fromInclusive, toExclusive);
        }

        public Location genLocation()
        {
            Location position = new Location();
            position.Latitude = random.NextDouble() * 180.0 - 90.0;
            position.Longitude = random.NextDouble() * 360.0 - 180.0;

            return position;
        }
    }
}
